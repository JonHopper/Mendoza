#define _DEFAULT_SOURCE

#include <iostream>

#include <unistd.h>
#include <signal.h>

#include "net/NetServer.hpp"
#include "net/POPProtocolBuffer.hpp"

static NetServer	*s_server;

void	sigint_handler(int sig __attribute__((unused)))
{
  s_server->stop();
  exit(0);
}

int	main()
{

  s_server = new NetServer(10110);

  ANetProtocolBuffer	*pbuf = new POPProtocolBuffer();

  s_server->registerProtocolBuffer(pbuf);

  if (!s_server->start())
    return 1;

  signal(SIGINT, &sigint_handler);

  while (true)
    {
      usleep(1500);
      s_server->update();
    }

  delete pbuf;
  return 0;
}
