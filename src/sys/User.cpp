#include <pwd.h>

#include"sys/User.hpp"

namespace Sys
{
  bool				User::exists(std::string const &login)
  {
    if (getpwnam(login.c_str()) == NULL)
      return false;
    return true;
  }
}
