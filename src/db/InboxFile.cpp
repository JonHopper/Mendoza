#include <iostream>
#include <sstream>
#include <fstream>
#include <exception>

#include <cstring>
#include <ctime>

#include "db/InboxFile.hpp"

InboxFile::InboxFile(std::string const &path, std::string const &username)
  : _path(path),
    _mutex(path, false),
    _nextMailId(1)
{

  std::ifstream		istream(path);

  if (!istream.is_open())
    init(path, username);
  else
    parse(path);

  istream.close();
}

InboxFile::~InboxFile()
{
  if (_header)
    delete _header;

  if (_mails.size() > 0)
    {
      for (std::map<unsigned int, MailObject>::iterator it = _mails.begin(); it != _mails.end(); ++it)
	delete std::get<0>(it->second);
    }
}

std::string const		InboxFile::getMailSubject(unsigned int const mailId) const
{
  if (mailId > _mails.size())
    return nullptr;

  return std::string(std::get<0>(_mails.at(mailId))->_subject);
}

std::string const		InboxFile::getMailSender(unsigned int const mailId) const
{
  if (mailId > _mails.size())
    return nullptr;

  return std::string(std::get<0>(_mails.at(mailId))->_sender);
}

std::string const		InboxFile::getMailRecipient(unsigned int const mailId) const
{
  if (mailId > _mails.size())
    return nullptr;

  return std::string(std::get<0>(_mails.at(mailId))->_recipient);
}

std::string const		InboxFile::getMailContent(unsigned int const mailId) const
{
  if (mailId > _mails.size())
    return nullptr;

  return std::string(std::get<1>(_mails.at(mailId)));
}

time_t				InboxFile::getMailTimestamp(unsigned int const mailId) const
{
  if (mailId > _mails.size())
    return 0;

  return std::get<0>(_mails.at(mailId))->_timestamp;
}

std::string			InboxFile::exportMail(unsigned int const mailId) const
{
  if (mailId > _mails.size())
    return "";

  std::stringstream	mExport;

  time_t	timestamp;
  char		date[24];
  timestamp = getMailTimestamp(mailId);
  strftime(date, 24, "%d %b %y %H%M %z", localtime(&timestamp));
  mExport << "Date: " << std::string(date) << "\r\n"
	  << "From: " << getMailSender(mailId) << "\r\n"
	  << "Subject: " << getMailSubject(mailId) << "\r\n"
	  << "Sender: nguye_g's Mendoza SMTPD\r\n"
	  << "To: " << getMailRecipient(mailId) << "\r\n"
	  << std::get<1>(_mails.at(mailId)) << "\r\n";

  return mExport.str();
}

int				InboxFile::addMail(std::string const &sender, std::string const &recipient,
						   std::string const &subject, std::string const &content)
{
  if (sender.length() >= 64 or recipient.length() >= 64 or subject.length() >= 64)
    return -1;

  MailHeader	*mailHeader = new MailHeader;

  memset(mailHeader, 0, sizeof(MailHeader));
  strncpy(mailHeader->_sender, sender.c_str(), 64);
  strncpy(mailHeader->_recipient, recipient.c_str(), 64);
  strncpy(mailHeader->_subject, subject.c_str(), 64);
  mailHeader->_timestamp = time(nullptr);
  mailHeader->_size = content.length();

  return addMail(mailHeader, content);
}

int				InboxFile::addMail(MailHeader *mailHeader, std::string const &content)
{
  _header->_mailNb += 1;
  _mails.emplace(_nextMailId++, std::make_tuple(mailHeader, content));

  _isFlushed = false;

  flush();

  return _mails.size() - 1;
}

bool				InboxFile::delMail(unsigned int const mailId)
{
  if (mailId > _mails.size())
    return false;

  _mails.erase(mailId);
  _header->_mailNb -= 1;

  _isFlushed = false;

  return flush();
}

unsigned int			InboxFile::getMailNb() const
{
  return _nextMailId - 1;
}

std::map<unsigned int, std::string>		*InboxFile::listMails() const
{
  std::map<unsigned int, std::string>	*list = new std::map<unsigned int, std::string>();

  for (std::map<unsigned int, MailObject>::const_iterator it = _mails.cbegin(); it != _mails.cend(); ++it)
    list->emplace(it->first, exportMail(it->first));

  return list;
}

bool				InboxFile::init(std::string const &filePath, std::string const &username)
{
  std::cout << "Initializing " << username << "'s inbox..." << std::endl;

  _header = new InboxHeader;

  memset(_header, 0, sizeof(InboxHeader));
  _header->_magic = MENDOZA_INBOX_MAGIC;
  _header->_mailNb = 0;
  strncpy(_header->_username, username.c_str(), 16);

  _mutex.lock();
  std::ofstream		initStream(filePath);
  initStream.write((char *)_header, sizeof(InboxHeader));

  initStream.flush();
  initStream.close();

  _mutex.unlock();

  return true;
}

bool				InboxFile::parse(std::string const &filePath)
{
  _header = new InboxHeader;
  memset(_header, 0, sizeof(InboxHeader));

  _mutex.lock();
  std::ifstream		readStream(filePath);
  readStream.read((char *)_header, sizeof(InboxHeader));

  if (_header->_magic != MENDOZA_INBOX_MAGIC)
    {
      std::cerr << "Invalid inbox file..." << std::endl;
      return false;
    }

  std::cout << "Parsing " << _header->_username << "'s inbox...\t";
  unsigned int		mailNb = 0;
  while (readStream.good() and mailNb < _header->_mailNb)
    {
      MailHeader	*mailHeader = new MailHeader;

      memset(mailHeader, 0, sizeof(MailHeader));
      readStream.read((char *)mailHeader, sizeof(MailHeader));

      if (!readStream.good())
	{
	  std::cerr << "read error " << readStream.tellg() << std::endl;
	  return false;
	}

      char		*mailContent = new char[mailHeader->_size + 1];

      memset(mailContent, 0, mailHeader->_size + 1);
      readStream.read(mailContent, mailHeader->_size);

      _mails.emplace(_nextMailId++, std::make_tuple(mailHeader, mailContent));

      delete [] mailContent;

      ++mailNb;
    }
  readStream.close();
  _mutex.unlock();

  std::cout << mailNb << " mail(s) readed" << std::endl;

  return true;
}

bool				InboxFile::flush()
{
  if (_isFlushed)
    return true;

  _mutex.lock();
  std::ofstream		writeStream(_path, std::fstream::out | std::fstream::trunc);
  writeStream.write((char *)_header, sizeof(InboxHeader));

  if (_header->_mailNb > 0)
    {
      for (std::map<unsigned int, MailObject>::const_iterator it = _mails.cbegin(); it != _mails.end(); ++it)
	{
	  MailHeader	*mailHeader = std::get<0>(it->second);

	  writeStream.write((const char *)mailHeader, sizeof(MailHeader));
	  writeStream.write(std::get<1>(it->second).c_str(), mailHeader->_size);
	}
    }

  writeStream.flush();
  writeStream.close();
  _mutex.unlock();

  std::cout << _header->_username << "'s inbox save complete: " << _header->_mailNb << " mail(s) stored." << std::endl;

  _isFlushed = true;
  return true;
}
