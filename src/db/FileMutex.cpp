#include <iostream>
#include <exception>

#include <unistd.h>
#include <fcntl.h>
#include <libgen.h>

#include "db/FileMutex.hpp"

FileMutex::FileMutex(std::string const &filePath, bool const semClean = false)
  :_semName("/MZIN_" + std::string(basename((char *)(filePath.c_str()))))
{
  if (semClean)
    sem_unlink(_semName.c_str());

  if (access((std::string("/dev/shmsem.") + _semName.substr(1)).c_str(), F_OK) != 0)
    _sem = sem_open(_semName.c_str(), O_CREAT, 0644, 1);
  else
    _sem = sem_open(_semName.c_str(), 0);

  if (_sem == SEM_FAILED)
    {
      std::cerr << "Cannot create/get semaphore for Inbox file '" << filePath << "'" << std::endl;
	throw std::exception();
    }
}

FileMutex::~FileMutex()
{
  sem_close(_sem);
}

int			FileMutex::lock()
{
  return sem_wait(_sem);
}

int			FileMutex::tryLock()
{
  return sem_trywait(_sem);
}

int			FileMutex::unlock()
{
  return sem_post(_sem);
}
