#include <iostream>
#include <fstream>
#include <sstream>
#include <openssl/md5.h>

#include "db/POPClient.hpp"

POPClient::POPClient(std::string const &login, std::string const &uid)
  : _login(login),
    _uid(uid)
{
  std::ifstream		confFile("popd.conf");

  if (!confFile.is_open())
    {
      std::cerr << "Cannot open 'popd.conf'" << std::endl;
      return;
    }

  bool	fileIsValid = false;
  char	line[1024];
  confFile.getline(line, 1023);

  while (confFile.good())
    {
      std::string	lineStr(line);

      if (lineStr.compare(1, 1, "#") == 0)
	{
	  confFile.getline(line, 1023);
	  continue;
	}

      if (!fileIsValid)
	{
	  if (lineStr.compare("users:") == 0)
	    fileIsValid = true;
	}
      else if (lineStr.compare(0, login.length(), login) == 0)
	{
	  _pass = lineStr.substr(login.length() + 1);

	  confFile.close();
	  return;
	}

      confFile.getline(line, 1023);
    }

  if (confFile.eof())
    {
      if (fileIsValid)
	std::cerr << "No such user '" << login << "'" << std::endl;
      else
	std::cerr << "Invalid configuration file" << std::endl;
    }
  else
    std::cerr << "Error while reading configuration file" << std::endl;

  confFile.close();
}

POPClient::~POPClient()
{
}

bool					POPClient::isValid() const
{
  if (_pass.length() < 1)
    return false;

  return true;
}

std::string const			POPClient::sessionId() const
{
  return std::string("<" + _uid + "@nguye_g.mendoza.epitech.eu>");
}

bool					POPClient::auth(std::string const &md5Token) const
{
  return (md5Token.compare(computeMD5Digest()) == 0);
}

bool					POPClient::textAuth(std::string const &password) const
{
  return (_pass.compare(password) == 0);
}

std::string const			POPClient::computeMD5Digest() const
{
  unsigned char		MD5Digest[MD5_DIGEST_LENGTH];
  std::string		passStr(sessionId() + _pass);

  MD5_CTX		context;
  MD5_Init(&context);
  MD5_Update(&context, passStr.c_str(), passStr.length());
  MD5_Final(MD5Digest, &context);

  static char const	hexDigits[17] = "0123456789abcdef";
  char			hexDigest[MD5_DIGEST_LENGTH * 2 + 1];

  for (unsigned int i = 0; i < MD5_DIGEST_LENGTH; ++i)
    {
      hexDigest[i * 2] = hexDigits[(MD5Digest[i] >> 4) & 0xF];
      hexDigest[i * 2 + 1] = hexDigits[MD5Digest[i] & 0xF];
    }
  hexDigest[MD5_DIGEST_LENGTH * 2] = '\0';

  return std::string(hexDigest);
}
