#include <iostream>
#include <exception>

#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include <cstdlib>
#include <cstring>

#include "net/NetServer.hpp"

NetServer::NetServer(unsigned short const port)
  : _port(port),
    _listenSocket(-1),
    _started(false),
    _nextClientId(0)
{
}

NetServer::~NetServer()
{
  stop();
}

bool					NetServer::start()
{
  std::cout << "[Network] Starting server..." << std::endl;

  try
    {
      initSocket("0.0.0.0", 10);
    }
  catch (std::exception &ex)
    {
      std::cerr << "[Network] Server failed to start." << std::endl;
      return false;
    }

  _started = true;
  std::cout << "[Network] Server started." << std::endl;
  return true;
}

void					NetServer::stop()
{
  std::cout << "[Network] Stopping server..." << std::endl;
  for (std::map<unsigned int, int>::iterator it = _clients.begin(); it != _clients.end(); ++it)
    close(it->second);

  _clients.clear();

  _started = false;
  std::cout << "[Network] Server stopped." << std::endl;
}

bool					NetServer::registerProtocolBuffer(ANetProtocolBuffer *pbuffer)
{
  if (!pbuffer)
    return false;

  _protocolBuffer = pbuffer;
  return true;
}

void					NetServer::update()
{
  receive();

  NetStrPacket	*outPacket = new NetStrPacket(-1, "");

  if (_protocolBuffer->sentPacket(outPacket))
    sendClient(outPacket);

  delete outPacket;
}

void					NetServer::initSocket(std::string const &ip, int const queue_size)
{
  if ((_listenSocket = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
      std::cerr << "[Network] socket error" << std::endl;
      throw std::exception();
    }

  socklen_t		slen;
  struct sockaddr_in	sockinfo;
  slen = sizeof(struct sockaddr_in);

  sockinfo.sin_family = AF_INET;
  sockinfo.sin_port = htons(_port);
  if (inet_aton(ip.c_str(), &sockinfo.sin_addr) == 0)
    {
      std::cerr << "[Network] Invalid IP bind address" << std::endl;
      close(_listenSocket);
      throw std::exception();
    }

  if (bind(_listenSocket, reinterpret_cast<struct sockaddr *>(&sockinfo), slen) == -1)
    {
      std::cerr << "[Network] bind error" << std::endl;
      close(_listenSocket);
      throw std::exception();
    }

  if (listen(_listenSocket, queue_size) == -1)
    {
      std::cerr << "[Network] bind error" << std::endl;
      close(_listenSocket);
      throw std::exception();
    }
}

int					NetServer::acceptClient()
{
  int			clientSocket;
  struct sockaddr_in	clientInfo;

  socklen_t		slen;
  slen = sizeof(struct sockaddr_in);

  if ((clientSocket = accept(_listenSocket, (struct sockaddr *)(&clientInfo), &slen)) == -1)
    {
      std::cerr << "[Network] accept error" << std::endl;
      return -1;
    }

  _clients.emplace(_nextClientId++, clientSocket);
  return _nextClientId - 1;
}

void					NetServer::disconnectClient(unsigned int const clientId)
{
  close(_clients[clientId]);
  _protocolBuffer->disconnectClient(clientId);
  _clients.erase(clientId);
}

void					NetServer::receive()
{
  int                   sret;
  int			maxFd;
  fd_set                fds;
  struct timeval        tv;

  FD_ZERO(&fds);
  FD_SET(_listenSocket, &fds);
  for (std::map<unsigned int, int>::iterator it = _clients.begin(); it != _clients.end(); ++it)
    FD_SET(it->second, &fds);

  memset(&tv, 0, sizeof(struct timeval));
  maxFd = (_clients.empty() ? _listenSocket : _clients.rbegin()->second) + 1;
  if ((sret = select(maxFd, &fds, NULL, NULL, &tv)) < 0)
    {
      std::cerr << "[Network] select error" << std::endl;
      return;
    }

  if (sret == 0)
    return;

  if (FD_ISSET(_listenSocket, &fds))
    _protocolBuffer->connectClient(acceptClient());

  for (std::map<unsigned int, int>::iterator it = _clients.begin(); it != _clients.end(); ++it)
    {
      if (FD_ISSET(it->second, &fds))
	{
	  if (!readClient(it->first))
	    break;
	}
    }
}

bool					NetServer::readClient(unsigned int clientId)
{
  int			rbytes;
  char			rbuffer[1024];

  if ((rbytes = read(_clients[clientId], rbuffer, 1023)) < 1)
    {
      if (errno != 0)
	std::cerr << "read error on client #" << clientId << std::endl;
      disconnectClient(clientId);

      return false;
    }
  rbuffer[rbytes] = '\0';

  NetStrPacket		*newPacket = new NetStrPacket(clientId, std::string(rbuffer));

  _protocolBuffer->receivePacket(newPacket);

  return true;
}

bool					NetServer::sendClient(NetStrPacket *packet)
{
  std::string		pdata = packet->getContent();

  if (write(_clients[packet->clientId()], pdata.c_str(), pdata.size()) < (ssize_t)pdata.size())
    {
      std::cerr << "write error on client #" << packet->clientId() << std::endl;

      return false;
    }

  if (pdata.compare(0, 3, "221") == 0 or (pdata.length() > 55 and pdata.compare(51, 3, "off") == 0))
    disconnectClient(packet->clientId());

  return true;
}
