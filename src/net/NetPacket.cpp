#include "net/NetPacket.hpp"

template <typename T>
NetPacket<T>::NetPacket(unsigned int const clientId, T const &content)
  : _packet(nullptr)
{
  _packet = new std::tuple<unsigned int, T>(clientId, content);
}

template <typename T>
NetPacket<T>::NetPacket(NetPacket const &oth)
  : _packet(nullptr)
{
  _packet = new std::tuple<unsigned int, T>(oth.clientId(), oth.getContent());
}

template <typename T>
NetPacket<T>::~NetPacket()
{
  delete _packet;
}

template <typename T>
size_t					NetPacket<T>::size() const
{
  return std::get<1>(*_packet).size();
}

template <typename T>
unsigned int				NetPacket<T>::clientId() const
{
  return std::get<0>(*_packet);
}

template <>
std::string const			&NetPacket<std::string>::getContent() const
{
  return std::get<1>(*_packet);
}

template<>
std::vector<char> const			&NetPacket<std::vector<char> >::getContent() const
{
  return std::get<1>(*_packet);
}


template <typename T>
char					&NetPacket<T>::operator[](size_t n)
{
  return std::get<1>(*_packet)[n];
}

template <typename T>
NetPacket<T>				&NetPacket<T>::operator=(NetPacket<T> const &oth)
{
  delete _packet;

  _packet = new std::tuple<unsigned int, T>(oth.clientId(), oth.getContent());

  return *this;
  }

template class NetPacket<std::string>;
template class NetPacket<std::vector<char> >;
