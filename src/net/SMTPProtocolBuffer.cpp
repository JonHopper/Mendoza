#include <iostream>

#include <cstring>

#include "net/SMTPProtocolBuffer.hpp"
#include "sys/User.hpp"

SMTPProtocolBuffer::SMTPProtocolBuffer()
  : ANetProtocolBuffer()
{
}

SMTPProtocolBuffer::~SMTPProtocolBuffer()
{
}

bool			SMTPProtocolBuffer::onSendProtocol()
{
  return false;
}

void			SMTPProtocolBuffer::onReceiveProtocol()
{
  NetStrPacket		packet = _inputBuffer.back();
  _inputBuffer.pop_back();

  QUIT(packet.clientId(), packet.getContent());

  switch (_clients[packet.clientId()]._status)
    {
    case CS_CONNECTED:
      HELO(packet.clientId(), packet.getContent());
      break;
    case CS_HELLOED:
      MAILFROM(packet.clientId(), packet.getContent());
      break;
    case CS_HASFROM:
      RCPTTO(packet.clientId(), packet.getContent());
      break;
    case CS_HASRCPT:
      RCPTTO(packet.clientId(), packet.getContent());
      DATA(packet.clientId(), packet.getContent());
      break;
    case CS_WAITCNT:
      CONTENT(packet.clientId(), packet.getContent());
      break;
    }
}

void			SMTPProtocolBuffer::onConnectClient(unsigned int const clientId)
{
  std::cout << "New client connected ! ID: #" << clientId << std::endl;

  ClientInfo		newClient;

  newClient._status = CS_CONNECTED;
  newClient._mailInfo = new InboxFile::MailHeader;
  memset(newClient._mailInfo, 0, sizeof(InboxFile::MailHeader));

  _clients.emplace(clientId, newClient);
  queuePacket(clientId, "220 nguye_g.mendoza.epitech.eu. ESMTP Mendoza\r\n");
}

void			SMTPProtocolBuffer::onDisconnectClient(unsigned int const clientId)
{
  std::cout << "Client #" << clientId << " disconnected !" << std::endl;
  _clients.erase(clientId);
}


void			SMTPProtocolBuffer::QUIT(unsigned int const clientId, std::string const &content)
{
  if (content.compare(0, 4, "QUIT") != 0)
    return;

  queuePacket(clientId, "221 2.0.0 Bye\r\n");
}

void			SMTPProtocolBuffer::HELO(unsigned int const clientId, std::string const &content)
{
  if (content.compare(0, 5, "HELO ") != 0 and content.compare(0, 5, "EHLO ") != 0)
    return;

  std::cout << "HELLO from " << content.substr(5, content.size() - 7) << "[#" << clientId << "]" << std::endl;

  _clients[clientId]._status = CS_HELLOED;
  queuePacket(clientId, "250 OK nguye_g.mendoza.epitech.eu\r\n");
}

void			SMTPProtocolBuffer::MAILFROM(unsigned int const clientId, std::string const &content)
{
    if (content.compare(0, 10, "MAIL FROM:") != 0)
      return;

    std::string		mfrom = content.substr(10, content.size() - 12);

    if (mfrom.find('@') == std::string::npos or mfrom.find('.') == std::string::npos)
      {
	queuePacket(clientId, "501 Syntax error - Invalid mail address\r\n");
	return;
      }

    strncpy(_clients[clientId]._mailInfo->_sender, mfrom.c_str(), 64);

    _clients[clientId]._status = CS_HASFROM;
    queuePacket(clientId, "250 2.1.0 Ok\r\n");
}


void			SMTPProtocolBuffer::RCPTTO(unsigned int const clientId, std::string const &content)
{
  if (content.compare(0, 8, "RCPT TO:") != 0)
    return;

  std::string account = content.substr(9, content.size() - 11);

  size_t	atPos;
  if ((atPos = account.find('@')) == std::string::npos)
    {
      queuePacket(clientId, "501 Syntax error - Invalid mail address\r\n");
      return;
    }

  if (account.substr(atPos + 1, account.size()).compare("nguye_g.mendoza.epitech.eu") != 0)
    {
      queuePacket(clientId, "501 Syntax error - Invalid domain\r\n");
      return;
    }

  if (!Sys::User::exists(account.substr(0, atPos)))
    {
      queuePacket(clientId, "550 No such user here\r\n");
      return;
    }

  strncpy(_clients[clientId]._mailInfo->_recipient, account.c_str(), 64);

  _clients[clientId]._status = CS_HASRCPT;
  queuePacket(clientId, "250 2.1.5 Ok\r\n");
}

void			SMTPProtocolBuffer::DATA(unsigned int const clientId, std::string const &content)
{
  if (content.compare(0, 4, "DATA") != 0)
    return;

  _clients[clientId]._status = CS_WAITCNT;
  queuePacket(clientId, "354 End data with <CR><LF>.<CR><LF>\r\n");
}

void			SMTPProtocolBuffer::CONTENT(unsigned int const clientId, std::string const &content)
{
  // 0 => Is first line
  // 1 => Has first <CR><LF>
  static bool	has[2] = {true, false};

  if (has[0])
    {
      getSubject(clientId, content.substr(0, content.size() - 2));
      has[0] = false;
      return;
    }

  if (!has[1])
    {
      if (content.compare("\r\n") == 0)
	has[1] = true;
      else
	_clients[clientId]._mailContent.append(content);

      return;
    }

  if (content.compare(".\r\n") == 0)
    storeMail(clientId);
  else
    {
      _clients[clientId]._mailContent.append(content);
      has[1] = false;
    }
}

bool			SMTPProtocolBuffer::getSubject(unsigned int const clientId, std::string const &content)
{
  if (content.compare(0, 9, "Subject: ") != 0)
    {
      strncpy(_clients[clientId]._mailInfo->_subject, "", 1);
      return false;
    }

  strncpy(_clients[clientId]._mailInfo->_subject, content.substr(9, content.size() - 9).c_str(), 64);
  return true;
}

void			SMTPProtocolBuffer::storeMail(unsigned int const clientId)
{
  _clients[clientId]._mailInfo->_timestamp = time(nullptr);
  _clients[clientId]._mailInfo->_size = _clients[clientId]._mailContent.length();

  std::string	username;

  username = _clients[clientId]._mailInfo->_recipient;
  username = username.substr(0, username.find('@'));

  InboxFile	*inbox = new InboxFile("./" + username + ".ibx", username);

  inbox->addMail(_clients[clientId]._mailInfo, _clients[clientId]._mailContent);

  delete inbox;

  queuePacket(clientId, "250 2.0.0 Ok: mail queued\r\n");

  _clients.erase(clientId);

  ClientInfo		client;
  client._status = CS_HELLOED;
  client._mailInfo = new InboxFile::MailHeader;
  memset(client._mailInfo, 0, sizeof(InboxFile::MailHeader));

  _clients.emplace(clientId, client);
}
