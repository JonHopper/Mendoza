#include <iostream>
#include <sstream>

#include <unistd.h>
#include <cstring>

#include "sys/User.hpp"
#include "db/POPClient.hpp"
#include "net/POPProtocolBuffer.hpp"

POPProtocolBuffer::POPProtocolBuffer()
  : ANetProtocolBuffer()
{
}

POPProtocolBuffer::~POPProtocolBuffer()
{
}

bool			POPProtocolBuffer::onSendProtocol()
{
  return false;
}

void			POPProtocolBuffer::onReceiveProtocol()
{
  NetStrPacket		packet = _inputBuffer.back();
  _inputBuffer.pop_back();

  QUIT(packet.clientId(), packet.getContent());

  switch (_clients[packet.clientId()]._status)
    {
    case CS_AUTHORIZATION:
      APOP(packet.clientId(), packet.getContent());
      USER(packet.clientId(), packet.getContent());
      PASS(packet.clientId(), packet.getContent());
      break;
    case CS_TRANSACTION:
      STAT(packet.clientId(), packet.getContent());
      LIST(packet.clientId(), packet.getContent());
      RETR(packet.clientId(), packet.getContent());
      DELE(packet.clientId(), packet.getContent());
      break;
    }
}

void			POPProtocolBuffer::onConnectClient(unsigned int const clientId)
{
  std::cout << "New client connected ! ID: #" << clientId << std::endl;

  time_t		now;
  std::stringstream	uid;

  time(&now);
  uid << getpid() << "." << now;

  ClientInfo		newClient;

  newClient._status = CS_AUTHORIZATION;
  newClient._uid = uid.str();
  _clients.emplace(clientId, newClient);

  queuePacket(clientId, std::string("+OK POP3 server ready <") + newClient._uid  + std::string("@nguye_g.mendoza.epitech.eu>\r\n"));
}

void		        POPProtocolBuffer::onDisconnectClient(unsigned int const clientId)
{
  std::cout << "Client #" << clientId << " disconnected !" << std::endl;
  _clients.erase(clientId);
}


void			POPProtocolBuffer::QUIT(unsigned int const clientId, std::string const &content)
{
  if (content.compare(0, 4, "QUIT") != 0)
    return;

  if (_clients[clientId]._status == CS_AUTHORIZATION)
    {
      queuePacket(clientId, "+OK nguye_g.mendoza.epitech.eu POP3 server signing off\r\n");
      return;
    }

  InboxFile	inbox(_clients[clientId]._login + ".ibx", _clients[clientId]._login);
  unsigned int	mailNb;

  mailNb = inbox.getMailNb();

  std::stringstream	sendPacket;
  sendPacket << "+OK nguye_g.mendoza.epitech.eu POP3 server signing off (";
  if (mailNb <= 0)
    sendPacket << "maildrop empty";
  else if (mailNb == 1)
    sendPacket << "1 message left";
  else
    sendPacket << mailNb << " messages left";
  sendPacket << ")\r\n";

  queuePacket(clientId, sendPacket.str());
}

void			POPProtocolBuffer::APOP(unsigned int const clientId, std::string const &content)
{
  if (content.compare(0, 4, "APOP") != 0)
    return;

  std::string		login;
  std::string		md5Token;
  std::stringstream	inPacket(content.substr(4, content.length() - 6));

  inPacket >> login >> md5Token;

  POPClient	client(login, _clients[clientId]._uid);

  if (!client.isValid())
    {
      queuePacket(clientId, "-ERR permission denied\r\n");
      return;
    }

  if (!client.auth(md5Token))
    {
      queuePacket(clientId, "-ERR permission denied\r\n");
      return;
    }

  _clients[clientId]._login = login;

  size_t				maildropSize;
  InboxFile				inbox(login + ".ibx", login);
  std::map<unsigned int, std::string>	*mailList = inbox.listMails();
  maildropSize = 0;
  for (std::map<unsigned int, std::string>::iterator it = mailList->begin(); it != mailList->end(); ++it)
    maildropSize += it->second.length();

  _clients[clientId]._status = CS_TRANSACTION;

  std::stringstream	sendPacket;
  sendPacket << "+OK " << login << "'s maildrop has " << mailList->size() << (mailList->size() > 1 ? " messages" : " message")
	     << " (" << maildropSize << " octets)\r\n";
  queuePacket(clientId, sendPacket.str());
}

void			POPProtocolBuffer::USER(unsigned int const clientId, std::string const &content)
{
  if (content.compare(0, 5, "USER ") != 0)
    return;

  std::string		login;
  std::stringstream	inPacket(content.substr(5, content.length() - 6));

  inPacket >> login;

  POPClient	client(login, _clients[clientId]._uid);

  if (!client.isValid())
    {
      queuePacket(clientId, "-ERR Unknown user\r\n");
      return;
    }

  _clients[clientId]._login = login;
  queuePacket(clientId, "+OK user registered\r\n");
}

void			POPProtocolBuffer::PASS(unsigned int const clientId, std::string const &content)
{
  if (content.compare(0, 5, "PASS ") != 0)
    return;

  if (_clients[clientId]._login.empty())
    {
      queuePacket(clientId, "-ERR use USER first\r\n");
      return;
    }

  POPClient	client(_clients[clientId]._login, _clients[clientId]._uid);

  std::string	password(content.substr(5, content.length() - 7));
  if (!client.textAuth(password))
    {
      queuePacket(clientId, "-ERR invalid password\r\n");
      return;
    }

  size_t				maildropSize;
  InboxFile				inbox(_clients[clientId]._login + ".ibx", _clients[clientId]._login);
  std::map<unsigned int, std::string>	*mailList = inbox.listMails();
  maildropSize = 0;
  for (std::map<unsigned int, std::string>::iterator it = mailList->begin(); it != mailList->end(); ++it)
    maildropSize += it->second.length();

  _clients[clientId]._status = CS_TRANSACTION;

  std::stringstream	outPacket;
  outPacket << "+OK " << _clients[clientId]._login << "'s maildrop has "
	    << mailList->size() << (mailList->size() > 1 ? " messages" : " message")
	    << " (" << maildropSize << " octets)\r\n";
  queuePacket(clientId, outPacket.str());
}

void			POPProtocolBuffer::CAPA(unsigned int const clientId, std::string const &content)
{
  if (content.compare(0, 4, "CAPA") != 0)
    return;

  queuePacket(clientId, "+OK Capability list follows\r\nUSER\r\n");
}

void			POPProtocolBuffer::STAT(unsigned int const clientId, std::string const &content)
{
  if (content.compare(0, 4, "STAT") != 0)
    return;

  size_t				maildropSize;
  InboxFile				inbox(_clients[clientId]._login + ".ibx", _clients[clientId]._login);
  std::map<unsigned int, std::string>	*mailList = inbox.listMails();

  maildropSize = 0;
  for (std::map<unsigned int, std::string>::iterator it = mailList->begin(); it != mailList->end(); ++it)
    maildropSize += it->second.length();


  std::stringstream	sendPacket;
  sendPacket << "+OK " << mailList->size() << " " << maildropSize << "\r\n";
  queuePacket(clientId, sendPacket.str());
}

void			POPProtocolBuffer::LIST(unsigned int const clientId, std::string const &content)
{
  if (content.compare(0, 4, "LIST") != 0)
    return;

  size_t				maildropSize;
  InboxFile				inbox(_clients[clientId]._login + ".ibx", _clients[clientId]._login);
  std::stringstream			listStr;
  std::map<unsigned int, std::string>	*mailList = inbox.listMails();

  maildropSize = 0;
  for (std::map<unsigned int, std::string>::iterator it = mailList->begin(); it != mailList->end(); ++it)
    {
      listStr << it->first << " " << it->second.length() << "\r\n";
      maildropSize += it->second.length();
    }
  listStr << ".\r\n";

  std::stringstream	sendPacket;
  sendPacket << "+OK " << mailList->size() << (mailList->size() > 1 ? " messages" : " message") << " (";
  if (maildropSize)
    sendPacket << maildropSize << " octets";
  else
    sendPacket << "maildrop empty";
  sendPacket << ")\r\n" << listStr.str();
  queuePacket(clientId, sendPacket.str());
}

void			POPProtocolBuffer::RETR(unsigned int const clientId, std::string const &content)
{
  if (content.compare(0, 5, "RETR ") != 0)
    return;

  std::stringstream			inPacket(content.substr(5, content.length() - 7));
  InboxFile				inbox(_clients[clientId]._login + ".ibx", _clients[clientId]._login);
  std::map<unsigned int, std::string>	*mailList = inbox.listMails();
  int					mailId;

  inPacket >> std::dec >> mailId;
  if (mailId < 1 or (unsigned int)mailId > mailList->size())
    {
      queuePacket(clientId, "-ERR no such mail");
      return;
    }

  std::stringstream	outPacket;
  outPacket << "+OK " << mailList->at(mailId).length() << " octets\r\n" << mailList->at(mailId) << ".\r\n";
  queuePacket(clientId, outPacket.str());
}

void			POPProtocolBuffer::DELE(unsigned int const clientId, std::string const &content)
{
  if (content.compare(0, 5, "DELE ") != 0)
    return;

  std::stringstream			inPacket(content.substr(5, content.length() - 7));
  InboxFile				inbox(_clients[clientId]._login + ".ibx", _clients[clientId]._login);
  int					mailId;

  inPacket >> std::dec >> mailId;
  if (mailId < 1 or (unsigned int)mailId > inbox.getMailNb())
    {
      queuePacket(clientId, "-ERR no such mail");
      return;
    }

  std::stringstream	outPacket;
  if (inbox.delMail(mailId))
    outPacket << "+OK message " << mailId  << " deleted\r\n";
  else
    outPacket << "-ERR I/O error\r\n";
  queuePacket(clientId, outPacket.str());
}

