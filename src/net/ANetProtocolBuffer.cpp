#include <iostream>

#include "net/ANetProtocolBuffer.hpp"

ANetProtocolBuffer::ANetProtocolBuffer()
{
}

ANetProtocolBuffer::~ANetProtocolBuffer()
{
}

bool				ANetProtocolBuffer::queuePacket(unsigned int const client, std::string const &content)
{
  _outputBuffer.push_front(NetStrPacket(client, content));

  return onSendProtocol();
}

bool				ANetProtocolBuffer::queuePacket(unsigned int const client, std::vector<char> const &content)
{
  std::cerr << "Invalid packet type send tentative on client " << client << std::endl
	    << "Converting to str packet..." << std::endl
	    << "WARNING: LOSS OF DATA RISK" << std::endl;

  _outputBuffer.push_front(NetStrPacket(client, std::string(content.begin(), content.end())));

  return onSendProtocol();
}

bool				ANetProtocolBuffer::sentPacket(NetStrPacket *packet)
{
  if (_outputBuffer.size() < 1)
    return false;

  *packet = _outputBuffer.back();
  _outputBuffer.pop_back();

  return true;
}

void				ANetProtocolBuffer::receivePacket(NetStrPacket const *packet)
{
  _inputBuffer.push_front(*packet);
  delete packet;
  onReceiveProtocol();
}

void				ANetProtocolBuffer::connectClient(unsigned int const clientId)
{
  onConnectClient(clientId);
}

void				ANetProtocolBuffer::disconnectClient(unsigned int const clientId)
{
  onDisconnectClient(clientId);
}
