#define _DEFAULT_SOURCE

#include <iostream>

#include <unistd.h>
#include <signal.h>

#include "db/InboxFile.hpp"
#include "net/NetServer.hpp"
#include "net/SMTPProtocolBuffer.hpp"

NetServer		*g_server;

void	sigint_handler(int sig __attribute((unused)))
{
  g_server->stop();
  exit(0);
}

int	main()
{

  g_server = new NetServer(10025);

  ANetProtocolBuffer	*pbuf = new SMTPProtocolBuffer();

  g_server->registerProtocolBuffer(pbuf);

  if (!g_server->start())
    return 1;

  signal(SIGINT, &sigint_handler);

  while (true)
    {
      usleep(1500);
      g_server->update();
    }

  delete pbuf;
  return 0;
}
