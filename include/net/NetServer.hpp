#ifndef MENDOZA_NETSERVER_HPP_
# define MENDOZA_NETSERVER_HPP_

# include <map>

# include "net/ANetProtocolBuffer.hpp"

class NetServer
{
private:
  unsigned short		_port;
  int				_listenSocket;
  bool				_started;
  std::map<unsigned int, int>	_clients;
  unsigned int			_nextClientId;
  ANetProtocolBuffer		*_protocolBuffer;

public:
  NetServer(unsigned short const port);
  ~NetServer();

  bool				start();
  void				stop();

  bool				registerProtocolBuffer(ANetProtocolBuffer *pbuffer);

  void				update();

private:
  void				initSocket(std::string const &ip, int const queue_size);
  int				acceptClient();
  void				disconnectClient(unsigned int const clientId);
  void				receive();
  bool				readClient(unsigned int clientId);
  bool				sendClient(NetStrPacket *packet);
};

#endif // !MENDOZA_NETSERVER_HPP_
