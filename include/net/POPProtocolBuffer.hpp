#ifndef MENDOZA_POPPROTOCOLBUFFER_HPP_
# define MENDOZA_POPPROTOCOLBUFFER_HPP_

# include <map>

# include "net/ANetProtocolBuffer.hpp"
# include "db/InboxFile.hpp"

class POPProtocolBuffer : public ANetProtocolBuffer
{
private:
  typedef enum
    {
      CS_AUTHORIZATION,
      CS_TRANSACTION
    }			ClientStatus;

    typedef struct
    {
      ClientStatus		_status;
      std::string		_login;
      std::string		_uid;
    }				ClientInfo;

  std::map<unsigned int, ClientInfo>		_clients;

public:
  POPProtocolBuffer();
  ~POPProtocolBuffer();

private:
  bool					onSendProtocol();
  void					onReceiveProtocol();
  void					onConnectClient(unsigned int const clientId);
  void					onDisconnectClient(unsigned int const clientId);

  void					QUIT(unsigned int const clientId, std::string const &content);
  void					APOP(unsigned int const clientId, std::string const &content);
  void					USER(unsigned int const clientId, std::string const &content);
  void					PASS(unsigned int const clientId, std::string const &content);
  void					CAPA(unsigned int const clientId, std::string const &content);
  void					STAT(unsigned int const clientId, std::string const &content);
  void					LIST(unsigned int const clientId, std::string const &content);
  void					RETR(unsigned int const clientId, std::string const &content);
  void					DELE(unsigned int const clientId, std::string const &content);
};

#endif // !MENDOZA_POPPROTOCOLBUFFER_HPP_
