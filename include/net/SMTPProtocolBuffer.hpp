#ifndef MENDOZA_SMTPPROTOCOLBUFFER_HPP_
# define MENDOZA_SMTPPROTOCOLBUFFER_HPP_

# include <map>

# include "net/ANetProtocolBuffer.hpp"
# include "db/InboxFile.hpp"

class SMTPProtocolBuffer : public ANetProtocolBuffer
{
private:
  typedef enum
    {
      CS_CONNECTED,
      CS_HELLOED,
      CS_HASFROM,
      CS_HASRCPT,
      CS_WAITCNT
    }			ClientStatus;

    typedef struct
    {
      ClientStatus		_status;
      InboxFile::MailHeader	*_mailInfo;
      std::string		_mailContent;
    }				ClientInfo;

  std::map<unsigned int, ClientInfo>		_clients;

public:
  SMTPProtocolBuffer();
  ~SMTPProtocolBuffer();

private:
  bool					onSendProtocol();
  void					onReceiveProtocol();
  void					onConnectClient(unsigned int const clientId);
  void					onDisconnectClient(unsigned int const clientId);

  void					QUIT(unsigned int const clientId, std::string const &content);
  void					HELO(unsigned int const clientId, std::string const &content);
  void					MAILFROM(unsigned int const clientId, std::string const &content);
  void					RCPTTO(unsigned int const clientId, std::string const &content);
  void					DATA(unsigned int const clientId, std::string const &content);
  void					CONTENT(unsigned int const clientId, std::string const &content);
  bool					getSubject(unsigned int const clientId, std::string const &content);
  void					storeMail(unsigned int const clientId);
};

#endif // !MENDOZA_SMTPPROTOCOLBUFFER_HPP_
