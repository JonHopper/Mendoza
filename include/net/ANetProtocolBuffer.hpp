#ifndef MENDOZA_ANETPROTOCOLBUFFER_HPP_
# define MENDOZA_ANETPROTOCOLBUFFER_HPP_

# include <list>

# include "net/NetPacket.hpp"

class ANetProtocolBuffer
{
protected:
  std::list<NetStrPacket>		_inputBuffer;
  std::list<NetStrPacket>		_outputBuffer;


public:
  virtual ~ANetProtocolBuffer();

  bool					queuePacket(unsigned int const client, std::string const &content);
  bool					queuePacket(unsigned int const client, std::vector<char> const &content);

  bool					sentPacket(NetStrPacket *packet);
  void					receivePacket(NetStrPacket const *packet);
  void					connectClient(unsigned int const clientId);
  void					disconnectClient(unsigned int const clientId);

protected:
  ANetProtocolBuffer();

private:
  virtual bool				onSendProtocol() = 0;
  virtual void				onReceiveProtocol() = 0;
  virtual void				onConnectClient(unsigned int const clientId) = 0;
  virtual void				onDisconnectClient(unsigned int const clientId) = 0;
};

#endif // !MENDOZA_ANETPROTOCOLBUFFER_HPP_
