#ifndef MENDOZA_NETPACKET_HPP_
# define MENDOZA_NETPACKET_HPP_

# include <vector>
# include <tuple>
# include <string>
# include <iterator>

template <typename T>
class NetPacket
{
private:
  std::tuple<unsigned int, T>		*_packet;

public:
  NetPacket(unsigned int const clientId, T const &content);
  NetPacket(NetPacket const &oth);
  ~NetPacket();

  size_t				size() const;
  unsigned int				clientId() const;
  T const				&getContent() const;


  char					&operator[](size_t n);
  NetPacket<T>				&operator=(NetPacket<T> const &oth);

};

typedef NetPacket<std::string>		NetStrPacket;
typedef NetPacket<std::vector<char> >	NetBinaryPacket;

#endif // !MENDOZA_NETPACKET_HPP_
