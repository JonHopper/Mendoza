#ifndef MENDOZA_SYS_USER_HPP_
# define MENDOZA_SYS_USER_HPP_

# include <string>

namespace Sys
{
  class User
  {
  public:
    static bool			exists(std::string const &login);
  };
}

#endif // !MENDOZA_SYS_USER_HPP_
