#ifndef MENDOZA_INBOXFILE_HPP_
# define MENDOZA_INBOXFILE_HPP_

# define MENDOZA_INBOX_MAGIC	0x4D5A494E

# include <string>
# include <vector>
# include <tuple>
# include <map>

# include "FileMutex.hpp"

class InboxFile
{
public:
  typedef struct __attribute__((packed))
  {
    char		_sender[64];
    char		_recipient[64];
    char		_subject[64];
    time_t		_timestamp;
    unsigned int	_size;
  }			MailHeader;

private:
  typedef struct __attribute__((packed))
  {
    unsigned int	_magic;
    unsigned int	_mailNb;
    char		_username[16];
  }			InboxHeader;

  typedef std::tuple<MailHeader *, std::string>		MailObject;

private:
  std::string const			_path;
  InboxHeader				*_header;
  FileMutex				_mutex;
  bool					_isFlushed;
  std::map<unsigned int, MailObject>	_mails;
  unsigned int				_nextMailId;

public:
  InboxFile(std::string const &path, std::string const &username);
  virtual ~InboxFile();

  std::string const			getMailSubject(unsigned int const mailId) const;
  std::string const			getMailSender(unsigned int const mailId) const;
  std::string const			getMailRecipient(unsigned int const mailId) const;
  std::string const			getMailContent(unsigned int const mailId) const;
  time_t				getMailTimestamp(unsigned int const mailId) const;
  std::string				exportMail(unsigned int const mailId) const;

  int					addMail(std::string const &sender, std::string const &recipient,
					std::string const &subject, std::string const &content);
  int					addMail(MailHeader *header, std::string const &content);
  bool					delMail(unsigned int const mailId);

  unsigned int				getMailNb() const;
  std::map<unsigned int, std::string>	*listMails() const;

private:
  InboxFile();
  InboxFile(InboxFile const &);
  InboxFile				&operator=(InboxFile const &);

  bool					init(std::string const &path, std::string const &username);
  bool					parse(std::string const &filePath);
  bool					flush();
};

#endif // !MENDOZA_INBOXFILE_HPP_
