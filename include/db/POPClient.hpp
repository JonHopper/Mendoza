#ifndef MENDOZA_POPCLIENT_HPP_
# define MENDOZA_POPCLIENT_HPP_

# include <string>

class POPClient
{
private:
  std::string const	_login;
  std::string		_uid;
  std::string		_pass;

public:
  POPClient(std::string const &login, std::string const &uid);
  ~POPClient();

  bool			isValid() const;
  std::string const	sessionId() const;
  bool			auth(std::string const &md5Token) const;
  bool			textAuth(std::string const &password) const;

private:
  std::string const	computeMD5Digest() const;
};

#endif // !MENDOZA_POPCLIENT_HPP_
