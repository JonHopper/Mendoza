#ifndef MENDOZA_FILEMUTEX_HPP_
# define MENDOZA_FILEMUTEX_HPP_

# include <string>

# include <semaphore.h>

class FileMutex
{
private:
  std::string const	_semName;
  sem_t			*_sem;

  FileMutex();
  FileMutex(FileMutex const &);
  FileMutex	&operator=(FileMutex const &);

public:
  FileMutex(std::string const &filePath, bool const semClean);
  virtual ~FileMutex();

  int			lock();
  int			tryLock();
  int			unlock();
};

#endif // !MENDOZA_FILEMUTEX_HPP_
