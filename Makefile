CC	= @g++
RM	= @rm -rvf
MD	= @mkdir -p
MAKE	= @make -s

ECHO	= @echo -e "\e[0;31;1m"
LECHO	= @echo -e "\e[0;33;1m"
CECHO	= @echo -e "\e[0;32;1m"
LBECHO	= @echo -e "\e[0;34;1m"
WCOLOR	= @echo -ne "\e[0;36;1m"
NCOLOR	= @echo -ne "\e[0m"

SRCDIR	= src
OBJDIR	= obj
INCDIR	= include

CFLAGS	+= -ansi -pedantic
CFLAGS	+= -Wall -Wextra
CFLAGS	+= -I./$(INCDIR) -std=c++11

LDFLAGS	= -lpthread -lcrypto

SMTPNAME = smtpd
POPNAME = popd

SMTPSRC	= smtp_main.cpp			\
	  sys/User.cpp			\
	  db/FileMutex.cpp		\
	  db/InboxFile.cpp		\
	  net/NetPacket.cpp		\
	  net/ANetProtocolBuffer.cpp	\
	  net/SMTPProtocolBuffer.cpp	\
	  net/NetServer.cpp

POPSRC	= pop_main.cpp			\
	  db/FileMutex.cpp		\
	  db/InboxFile.cpp		\
	  db/POPClient.cpp		\
	  net/NetPacket.cpp		\
	  net/ANetProtocolBuffer.cpp	\
	  net/POPProtocolBuffer.cpp	\
	  net/NetServer.cpp

SMTPOBJS = $(SMTPSRC:%.cpp=$(OBJDIR)/%.oxx)
POPOBJS	 = $(POPSRC:%.cpp=$(OBJDIR)/%.oxx)

all: 	$(SMTPNAME) $(POPNAME)

$(OBJDIR)/%.oxx:	$(SRCDIR)/%.cpp
	$(MD) $(OBJDIR)/sys $(OBJDIR)/db $(OBJDIR)/net
	$(ECHO) Compiling $<...
	$(NCOLOR)
	$(CC) -o $@ -c $< $(CFLAGS)

$(SMTPNAME):	$(SMTPOBJS)
	$(LECHO) Linking $(SMTPNAME)...
	$(NCOLOR)
	$(CC) -o $(SMTPNAME) $(SMTPOBJS) $(LDFLAGS)

$(POPNAME):	$(POPOBJS)
	$(LECHO) Linking $(POPNAME)...
	$(NCOLOR)
	$(CC) -o $(POPNAME) $(POPOBJS) $(LDFLAGS)

clean:
	$(CECHO) Cleaning object files...
	$(NCOLOR)
	$(RM) $(SMTPOBJS) $(POPOBJS)

fclean: clean
	$(CECHO) Cleaning binaries...
	$(NCOLOR)
	$(RM) $(SMTPNAME) $(POPNAME)

re: fclean all

.PHONY: all clean fclean re
